# 说明

此模板用于《笨方法学写作》学员提交课程作业。

使用方法如下：

## Fork 模板

打开 [cnfeat / cnfeat.gitlab.io · GitLab](https://gitlab.com/cnfeat/cnfeat.gitlab.io) 页面，点右上角 `Fork` 按键。

![etQ6PO.jpg](https://s2.ax1x.com/2019/07/31/etQ6PO.jpg)



## 改名字

左侧栏进入 Settings > General 

找到 Naming, topics, avatar

将 Project name 改成 `username.gitlab.io`   ( username 是你的 gitlab id )

![etQra6.jpg](https://s2.ax1x.com/2019/07/31/etQra6.jpg)

## 运行

左侧栏进入 CI/CD ，点击 `Run Pipeline`

点击下一步，完成余下操作，页面的 `Stages` 会显示在运行

![etQsIK.jpg](https://s2.ax1x.com/2019/07/31/etQsIK.jpg)

待运行图标变成绿色勾状图标，即表示页面已经部署完毕

## 完成

此时，打开 `username.gitlab.io`

就会显示你的个人作业主页。

## 提交作业


……











此模板 fork 自 [GitLab Pages examples / gitbook · GitLab](https://gitlab.com/pages/gitbook)