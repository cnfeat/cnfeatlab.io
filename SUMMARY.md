# 作业清单

* 1w
    * [1w 每周卡片](1w/card&weekly.md)
    * [1w 每周习题](1w/work.md)
* 2w
    * [2w 每周卡片](2w/card&weekly.md)
    * [2w 每周习题](2w/work.md)
* 3w
    * [3w 每周卡片](3w/card&weekly.md)
    * [3w 每周习题](3w/work.md)
* 4w
    * [4w 每周卡片](4w/card&weekly.md)
    * [4w 每周习题](4w/work.md)
* 5w
    * [5w 每周卡片](5w/card&weekly.md)
    * [5w 每周习题](5w/work.md)
* 6w
    * [6w 每周卡片](6w/card&weekly.md)
    * [6w 每周习题](6w/work.md)
* 7w
    * [7w 每周卡片](7w/card&weekly.md)
    * [7w 每周习题](7w/work.md)

